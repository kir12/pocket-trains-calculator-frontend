import React from 'react';

// Component for generating the dropdown widget.
// NOTE: due to API queries, a separate compnent was necessary.

class Dropdown_Query extends React.Component{
    constructor(props){
        super(props);
        this.state={
            entry_name : this.props.entry_name,
            api_endpoint : this.props.api_endpoint,
            parameters : this.props.parameters,
            prev_value : this.props.prev_value,
            on_change : this.props.on_change
        };
    }

    is_disabled(){
        return this.state.prev_value === " ";
    }

    render_options(){
        if(this.is_disabled()){
            return null;
        }
        if(this.state.options_raw){ //case 1: return value from API
            return Object.keys(this.state.options_raw).map((key) => <option key = {key}>{this.state.options_raw[key]}</option>);
            //return <option>a</option>;
        }
        else if(this.state.options_hard){// case 2: hard options supplied
            return Object.keys(this.options_hard).map((option) => <option>{option}</option>);
        }
    }

    componentDidMount(){
        if(!this.is_disabled()){
            fetch("http://127.0.0.1:8000/trains/" + this.state.api_endpoint)
                .then(res => res.json())
                .then(
                    (data) => {this.setState({options_raw : data})}
                );
        }
    }
    render(){
        return (
            <div className = "form-group row">
                <label className = "col-sm-4 col-form-label"> {this.state.entry_name} </label>
                <div className = "col-sm-8">
                    <select className="form-control" disabled ={this.is_disabled() ? true : null} onChange = {this.state.on_change}>
                        <option> </option>
                        {this.render_options()}
                    </select>
                </div>
            </div>
        );
    }
}

export default Dropdown_Query