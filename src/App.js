import React from 'react';
import './App.css';
import Track_Route_Data from './Track_Route_Data.js' ;

function App() {
  return (
    <div className = "container">
      <div className = "jumbotron text-center"><h1>pocket-trains-calculator</h1></div>

      {/* selection utility for changing option, will implement later*/}
      <ul className="nav nav-pills nav-fill">
        <li className="nav-item">
          <a className="nav-link " href="#">Home and About</a>
        </li>
        <li className="nav-item">
          <a className="nav-link active" href="#">Track Route Data</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Best Route For Train</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Track Data</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Train Data</a>
        </li>
      </ul>

      {/* app rendering goes here. 
      TODO: figure out how to change app based on user selection above.
      Ideal behavior: all loading happens at once and before user interaction begins. */}
      < Track_Route_Data/>
    </div>
  );
}

export default App;
