import React from 'react';
import Dropdown_Query from './Dropdown_Query.js'

class Track_Route_Data extends React.Component{

    constructor(props){
        super(props);
        this.state={
            selection_info: [" ", " "]
        };
        this.state.change_value = this.change_value.bind(this);
    }

    //TODO: subsequent entries not updating properly
    change_value(index, event){
        let old_selection_info = this.state.selection_info.slice();
        old_selection_info[index]=event.target.value;
        this.setState({selection_info:old_selection_info}, () => {
            console.log(this.state.selection_info[index])
        });
    }

    render(){
        return (
            <div>
                <h1>Track Route Data</h1>
                <p> Given locomotive and track route, determine if it can make a full trip, assess its coin cost, and various other useful track info.</p>
                <form>
                    <Dropdown_Query entry_name={"Select a Locomotive Class"} api_endpoint = {"locomotive_class"} on_change = {v => this.change_value(0,v)} prev_value={"a"}/>
                    <Dropdown_Query entry_name={"Select a Specific Locomotive"} api_endpoint={"locomotive_detail"} on_change={v => this.change_value(1,v)} prev_value={this.state.selection_info[0]}/>

                    <div className = "form-group row">
                        <label className = "col-sm-4 col-form-label"> Select a Specific Locomotive</label>
                        <div className = "col-sm-8">
                            <select className="form-control" disabled>
                                <option>a</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div className = "form-group row">
                        <label className = "col-sm-4 col-form-label"> Select a Fuel Car Configuration</label>
                        <div className = "col-sm-8">
                            <select className="form-control" disabled>
                                <option>a</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>

                    <hr />
                    <div className = "form-group row">
                        <label className = "col-sm-4 col-form-label"> Select a Continent</label>
                        <div className = "col-sm-8">
                            <select className="form-control">
                                <option>a</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div className = "form-group row">
                        <label className = "col-sm-4 col-form-label"> Select a Starting City</label>
                        <div className = "col-sm-8">
                            <select className="form-control" disabled>
                                <option>a</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>

                    {/* TODO: figure out how to dynamically populate form */}

                </form>
            </div>
        );
    }
}

export default Track_Route_Data